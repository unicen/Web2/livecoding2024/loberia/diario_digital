<?php

class NoticiasView {
    // "TUDAI - Diario Digital"
   public function mostrarHead($title) {
    $salida = ""; 

    $salida .= '<!DOCTYPE html>';
    $salida .= '<html lang="en">';
    $salida .= '<head>';
    $salida .= '    <meta charset="UTF-8">';
    $salida .= '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
    $salida .= '    <meta name="viewport" content="width=device-width, initial-scale=1.0">';
    $salida .= '    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">';
    $salida .= '    <link rel="stylesheet" href="css/style.css">';
    $salida .= '      <title>'.$title.'</title>';
    $salida .= '</head>';

    return $salida;
   } 

   public function mostrarFooter() {
    $salida = ""; 

    $salida .= '<footer class="d-flex flex-wrap justify-content-center align-items-center py-3 my-4 border-top">';
    $salida .= '<div class="align-items-center">';
    $salida .= '    <span class="text-muted">2024 TUDAI LOBERIA, UNICEN</span>';
    $salida .= '  </div>';
    $salida .= '</footer>';
  
    $salida .= '<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>';

    return $salida;
   } 

   private function mostrarHeader() {
    $salida = ""; 
    $salida .= '<header>';
    $salida .= '    <nav class="navbar navbar-expand-lg bg-light">';
    $salida .= '        <div class="container-fluid">';
    $salida .= '          <a class="navbar-brand" href="index.php">TUDAI Digital</a>';
    $salida .= '          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">';
    $salida .= '            <span class="navbar-toggler-icon"></span>';
    $salida .= '          </button>';
    $salida .= '          <div class="collapse navbar-collapse" id="navbarNav">';
    $salida .= '            <ul class="navbar-nav">';
    $salida .= '              <li class="nav-item">';
    $salida .= '                <a class="nav-link" aria-current="page" href="index.php">Noticias</a>';
    $salida .= '              </li>';
    $salida .= '            </ul>';
    $salida .= '          </div>';
    $salida .= '        </div>';
    $salida .= '      </nav>';
    $salida .= '</header>';

    return $salida;
   }

   public function mostrarBody($noticias) {
    $salida = ""; 

    $salida .= $this->mostrarHeader();

    $salida .= '<main class="container mt-5">';
    $salida .= '  <section class="noticias">';

    foreach($noticias as $index => $noticia){ 
        $salida .= '  <div class="card">';
        $salida .= '    <img src="'.$noticia->imagen.'" class="card-img-top" alt="...">';
        $salida .= '    <div class="card-body">';
        $salida .= '      <h5 class="card-title">'.utf8_encode($noticia->titulo).'</h5>';
        $salida .= '      <p class="card-text">'.utf8_encode($noticia->contenido).'</p>';
        $salida .= '      <a href="noticia.php?id='.$index.'" class="btn btn-outline-primary">Leer más</a>';
        $salida .= '    </div>';
        $salida .= '  </div>';
    }

    $salida .= '  </div>';
    $salida .= '    </section>';
    $salida .= '  </main>';

    return $salida;
   } 

   function mostrar($title, $noticias) {
    $salida = "";
    $salida .= $this->mostrarHead($title);
    $salida .= $this->mostrarBody($noticias);
    $salida .= $this->mostrarFooter();

    echo $salida;
   }
}