<?php
require_once(BASE_DIR.'/app/model/NoticiasModel.php');
require_once(BASE_DIR.'/app/view/NoticiasView.php');

class NoticiasController {

    private $view;
    private $model;

    function __construct() {
        $this->view = new NoticiasView();
        $this->model = new NoticiasModel();

    }

    function index() {
        $noticias = $this->model->getNoticias();
        $title = 'TUDAI - Diario Digital';

        $this->view->mostrar($title, $noticias);
    }

}