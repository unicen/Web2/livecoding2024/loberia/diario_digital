<?php 

class NoticiasModel {

    private $pDO;

    function __construct() {
        $servidor = 'mysqldb'; //localhost
        $base = 'noticias';
        $usuario = 'root';
        $password = 'tudai';  //root

        try {
            $this->pDO = new PDO("mysql:host=$servidor;dbname=$base", $usuario, $password);
        } catch (\Throwable $th) {
            var_dump($th);
        }
        
    }

    public function getNoticias() {
        
        $sql = "SELECT * FROM noticia";
        $query = $this->pDO->prepare($sql);

        $query->execute([]);

        $noticias = $query->fetchAll(PDO::FETCH_OBJ);

        return $noticias;
    }

    // private function insertarNoticias() {
    //     $noticias = $this->getNoticias();

    //     foreach ($noticias as $noticia) {
    //         $titulo = $noticia->titulo;
    //         $contenido = $noticia->contenido;
    //         $imagen = $noticia->imagen;

    //         $sql = "INSERT INTO noticia (titulo, contenido, imagen) VALUES (?, ?, ?)";

    //         $query = $this->pDO->prepare($sql);
    //         $query->execute([utf8_decode($titulo), 
    //         utf8_decode($contenido), 
    //                         $imagen]);

    //     }

    // }
}
